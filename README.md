### Start the csrf-demo-server which runs on port 8001
``` 
npm start
````

### Add the following nginx configuration
``` bash
server {
    listen 80;
    server_name demo.csrf.local demo.csrf.local;
    root /Users/njt/Dev/tmp/csrf-demo/client/public;

    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    index index.html index.htm index.php;

    charset utf-8;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location /api/ {
        proxy_pass http://localhost:8001;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}
```

### add the host name in /etc/host as
```
demo.csrf.local
```
### Flow
* Access the above in the browser as http://demo.csrf.local
* Click on the Call Ping and it returns a new XSRF-TOKEN every time it is clicked
* Click on Verify and it returns either 204 or 403
* The cookie is being is set in the Application storage

### Obsevations
* In the expressjs, do not set max age, a CSRF cookie expiry should be session age.
* While setting up cors, pass another option `credentials: true` 
    * `app.use(cors({ origin: true, credentials: true }));`