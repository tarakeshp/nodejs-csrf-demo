const express = require("express");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const csrf = require("csurf");
const cors = require("cors");
const app = new express();
const port = 8001;
const csrfProtection = csrf({ cookie: true });

app.use(cors({ origin: true, credentials: true }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.use(csrfProtection, (req, res, next) => {
    console.info(`XCSRF-TOKEN: ${req.headers['xsrf-token']}`);
    if (!req.headers['xsrf-token'] || !req.headers['x-xsrf-token']) {
        const oneHour = 3600 * 1000 // Our token expires after one hr
        res.header('Access-Control-Expose-Headers', 'Set-Cookie')
        res.cookie('XSRF-TOKEN', req.csrfToken(), {
          // maxAge: oneHour, // this is the culprit
          httpOnly: false,
          secure: false,
        });
        return next()
    }
    return next();
});

app.get("/", (req, res, next) => {
  res.redirect("/api/ping");
});

app.get("/api/ping", (req, res, next) => {
  res.json({
    server_time: Date.now(),
    server_name: "csrf-demo-server",
  });
});

app.post("/api/verify-csrf", csrfProtection, (req, res, next) => {
  res.status(204).json({});
});

app.use(function (err, req, res, next) {
    if (err.code !== 'EBADCSRFTOKEN') return next(err)
   
    // handle CSRF token errors here
    res.status(403)
    res.send('form tampered with');
  })

app.listen(port, () => {
  console.info(`http://localhost:${port}/`);
});
